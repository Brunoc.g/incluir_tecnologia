<?php 

$regioes = $_GET['regioes'];
$paises = $_GET['paises'];

if($paises !== "paises" && $regioes !== "bandeira"){

    echo buscarDadosPais($paises);

} else if($paises !== "paises" && $regioes === "bandeira"){

    echo buscarBandeira($paises);

} else if ($paises === "paises" && $regioes === "regioes"){

    echo buscarRegiao($regioes);

} else if ($paises === "paises" && $regioes === "paises"){

    echo buscarPais($regioes);

} else if ($paises === "paises" && $regioes !== "regioes"){

    echo buscarPais($regioes);

}

//FUNÇÃO DE UTILIZAR A API
function baixarAPI(){

    //Passando URL da API
    $url="https://restcountries.eu/rest/v2/all";

    //COnvertendo para Json ("Array")   
    $arrayApi = json_decode(file_get_contents($url));

    //retornando o arquivo ("array")
    return ($arrayApi);

};

//FUNÇÃO DE BUSCA DAS REGIÕES
function buscarRegiao($escolha){

    //Passando URL da API
    //$url="https://restcountries.eu/rest/v2/all";

    //COnvertendo para Json ("Array")   
    //$mundo = json_decode(file_get_contents($url));

    //$mundo = array();

    //Pegando o array da api
    $mundo = baixarAPI();

    //Caso nenhuma região tenha sido selecionada
    if((empty($escolha)) || ($escolha === "regioes")){

        //Pega o Tamanho do array convertido em Json
        $tamanhoArray = count($mundo);

        //Contador para inserção de novas regiões no array de regiões
        $contadorRegioes = 0;

        //Array para receber as regiões
        $arrayRegioes = array();

        //Loop de varredura de todo array
        for($i = 0; $i < $tamanhoArray; $i++){

            //Pega o tamanho do array recebe as diferentes regiões 
            $tamanhoArrayRegioes = count($arrayRegioes);

            //Primeiro loop coloca a região no array que recebe as regiões
            if($i === 0){
                $arrayRegioes[$i] = $mundo[$i]->region;
                $contadorRegioes++; 
            
            //Caso não seja o primeiro loop
            }else{

                //Variável que dirá se deve inserir uma nova região no array
                $novaRegiao = true;

                //Contador para varrer o array de regiões recebidas
                $j = 0;

                //Loop de varredura do array regiões
                while($j < $tamanhoArrayRegioes){

                    //Variável recebendo o valor contido no total
                    $teste1 = $mundo[$i]->region;                
                    
                    //Variável recebendo o valor contido no array de regiões
                    $teste2 = $arrayRegioes[$j];

                    //Comparando se a região contida no array total já existe no array de regiões
                    if($teste1 === $teste2 && !empty($teste1)){

                        $novaRegiao = false;

                    };

                    $j++;

                };

                //Condição que deve ser inserida uma nova região no array
                if ($novaRegiao === true){

                    $arrayRegioes[$contadorRegioes] = $mundo[$i]->region;
                    $contadorRegioes++;

                };

            };
        };
    
        //Pegando o tamanho do array de regiões populado
        $tamanhoArrayRegioes = count($arrayRegioes);   

        //Contado para varrer o array de regiões e pegar apenas os valores reais
        $x = 0;

        //Array para receber as regiões do mundo
        $regioesDoMundo = array();

        //Contador de inserção das regiões do mundo no array de retorno
        $z = 0;

        //Varrendo o array de regiões populado
        while($x < $tamanhoArrayRegioes){

            //Verificando se o dado no array é válido
            if(!empty($arrayRegioes[$x])){

                $regioesDoMundo[$z] = $arrayRegioes[$x];
                //$regioesDoMundo[] = pg_fetch_assoc($arrayRegioes, $x) ;
                $z++;
                
            };  

            $x++; 

        };

        //for($i = 0; $i<$n; $i++) { $dados[] = pg_fetch_assoc($result, $i); 

        //Colocando o array de retorno em ordem alfabética
        sort($regioesDoMundo);

        //retornando as regiões
        echo json_encode($regioesDoMundo, JSON_PRETTY_PRINT);
        //return ($regioesDoMundo);
    
    //Caso uma região tenha sido selecionada
    } else {

        //retornando o nome da região selecionada
        echo json_encode($escolha, JSON_PRETTY_PRINT);
        //return($escolha);

    };

};

//FUNÇÃO DE BUSCA DOS PAÍSES
function buscarPais($escolhaRegiao){

    //Passando URL da API
    //$url="https://restcountries.eu/rest/v2/all";

    //COnvertendo para Json ("Array")   
    //$planeta = json_decode(file_get_contents($url));

    //Pegando o array da api
    $planeta = baixarAPI();

    //Pegando o tamanho do arquivo convertido em json
    $tamanhoPlaneta = count($planeta);

    //Variável que receberá o nome dos países
    $paisesDoMundo = array();

    //Contador de inserção dos países no array
    $contadorPaises = 0;

    //Caso não tenha sido escolhido nenhuma região
    if (empty($escolhaRegiao) || $escolhaRegiao === "regioes" || $escolhaRegiao === "paises"){    
        
        //Contador de varredura do array total
        $y = 0;
        
        //loop de varredura do array
        while ($y < $tamanhoPlaneta){

            //Verificação de nome de país em português válido para inserção no array
            if(!empty($planeta[$y]->translations->br)){

                $paisesDoMundo[$contadorPaises] = $planeta[$y]->translations->br;
                $contadorPaises++;

            };

            $y++;

        };

        //Ordenando alfabeticamente os países
        sort($paisesDoMundo);

        //Retornando os países
        echo json_encode($paisesDoMundo, JSON_PRETTY_PRINT);
        //return($paisesDoMundo);

    //Caso uma região tenha sido escolhida
    } else{

        //Variável para varrer o array total
        $contadorPlaneta = 0;

        //Variável para receber os países da região selecionada
        $paisesRegiao = array();

        //Contador de inserção dos países da região selecionada
        $contadorArrayPaises = 0;

        //Loop de varredura do array total
        while($contadorPlaneta < $tamanhoPlaneta){
            
            //Comparação da região do pais para inserção no array
            if($planeta[$contadorPlaneta]->region === $escolhaRegiao){

                $paisesRegiao[$contadorArrayPaises] = $planeta[$contadorPlaneta]->translations->br;
                $contadorArrayPaises++;

            };

            $contadorPlaneta++;

        };

        //Ordenando alfabeticamente os países da região
        sort($paisesRegiao);

        //Retornando os países
        echo json_encode($paisesRegiao, JSON_PRETTY_PRINT);
        //return($paisesRegiao);

    };

};

//FUNÇÃO DE BUSCA DE DADOS DO PAÍS SELECIONADO
function buscarDadosPais($nomePais){

    //Pegando o array da api
    $paises = baixarAPI();

    //Pegando o tamanho do arquivo convertido em json
    $tamanhoPaises = count($paises);

    //Array de retorno
    $dadosPais = array();

    $nameVizinhos = "";

    //Contador de varredura do array total
    $cont = 0;

    //Contador dos dados do país
    $contDados = 0;

    //Varrendo o array para buscar os dados do pais escolhido
    while ($cont < $tamanhoPaises){

        //Comparando com o nome selecionado
        if($paises[$cont]->translations->br === $nomePais){
            
            //$dadosPais[$contDados] = file_get_contents($paises[$cont]->flag);
            //$bandeiraPais = $paises[$cont]->flag;
            //$bandeiraPaisPng = toPNG($bandeiraPais, 'sgv');
            $dadosPais[$contDados] = $paises[$cont]->flag;
            $contDados++;
            $dadosPais[$contDados] = $paises[$cont]->translations->br;
            $contDados++;
            $dadosPais[$contDados] = $paises[$cont]->capital;
            $contDados++;
            $dadosPais[$contDados] = $paises[$cont]->region;
            $contDados++;
            $dadosPais[$contDados] = $paises[$cont]->subregion;
            $contDados++;
            $dadosPais[$contDados] = $paises[$cont]->population;
            $contDados++;
            $dadosPais[$contDados] = $paises[$cont]->languages['0']->nativeName;
            $contDados++;
            
            $siglasBorders = $paises[$cont]->borders;
            $nomeBorders = buscarNomeVizinhos($siglasBorders);
            $dadosPais[$contDados] = $nomeBorders;
        };

        $cont++;

    };

    echo json_encode($dadosPais, JSON_PRETTY_PRINT);
        
};

//FUNÇÃO DE BUSCA DOS NOMES DOS VIZINHOS EM PT-BR
function buscarNomeVizinhos($vizinhos){

    //Pegando o array da api
    $arrayTotal = baixarAPI();

    //Pegando o tamanho do arquivo convertido em json
    $tamanhoArrayRecebida = count($arrayTotal);

    //Pegando a quantidade de vizinhos recebida
    $quantidadeVizinhos = count($vizinhos);

    //Contador de varredura do array total
    $indTotal = 0;

    //Contador para percorrer os vizinhos recebidos
    $indVizinhos = 0;

    //Variável de resposta dos nomes dos vizinhos
    $respostaNomesVizinhos = array();

    //Varrendo o array para buscar os dados do pais escolhido e for menor que a quantidade de vizinhos
    while($indTotal < $tamanhoArrayRecebida && $indVizinhos < $quantidadeVizinhos){

        //Compara o código do vizinho com o código recebido
        if ($arrayTotal[$indTotal]->alpha3Code === $vizinhos[$indVizinhos]){

           $respostaNomesVizinhos[$indVizinhos]=$arrayTotal[$indTotal]->translations->br;
           $indVizinhos++;

        };

        $indTotal++;

    };

    //Ordenando alfabeticamente os países vizinhos
    sort($respostaNomesVizinhos);

    //Retornando os nomes dos países vizinhos
    return($respostaNomesVizinhos);    

};

//FUNÇÃO DE BUSCA DO LINK DAS BANDEIRAS
function buscarBandeira ($nomePaisBandeira){

    //Pegando o array da api
    $bandeiraTotal = baixarAPI();

    //Pegando o tamanho do arquivo convertido em json
    $quantidadeBandeiras = count($bandeiraTotal);

    //Contador para varrer a array total
    $contBandeira = 0;

    //Loop no arquivo para procurar a bandeira
    while($contBandeira < $quantidadeBandeiras){

        //Comparando para pegar a bandeira do pais selecionada
        if($nomePaisBandeira === $bandeiraTotal[$contBandeira]->translations->br){
            
            //$imagemRecebida = file_get_contents($bandeiraTotal[$contBandeira]->flag);
            $imagemRecebida = $bandeiraTotal[$contBandeira]->flag;
        };

        $contBandeira++;

    };

    echo json_encode($imagemRecebida, JSON_PRETTY_PRINT);
    //echo json_encode($imagemRecebida);
    //return ($respostaLinkBandeira);

};

//FUNÇÃO DE PREENCHIMENTO AUTOMÁTICO
function retornaPaises(){

    $regiaoSelecionada = $_REQUEST['regioes'];
    $respostaPaisesDaRegiaoSelecionada = buscarPais($regiaoSelecionada);
    $quaaqntidadeDePaisesDaRegiaoSelecionada = count($respostaPaisesDaRegiaoSelecionada);
    $contadorPaisesRegiaoSelecionada = 0;

    while ($contadorPaisesRegiaoSelecionada < $quaaqntidadeDePaisesDaRegiaoSelecionada){

        $respostaPaisesDaRegiaoSelecionada['nomePais'] = $respostaPaisesDaRegiaoSelecionada[$contadorPaisesRegiaoSelecionada];
        $contadorPaisesRegiaoSelecionada++;

    };
	
	echo(json_encode($respostaPaisesDaRegiaoSelecionada));
};

?>