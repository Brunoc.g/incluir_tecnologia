$(document).ready(function(){
    $('#bandeiras_vizinhos').empty();
    $('#band_vizinhos').empty();
    let larguraTela = window.innerWidth;
    let quantidadeColunatabela = 0;
    if(larguraTela <= 921){
        quantidadeColunatabela = 0;
    } else {
        quantidadeColunatabela = 3;
    }
    $.getJSON('funcoes.php?regioes=regioes&paises='+nomePaisRecebido, function(dados){
        let paisSelecionado = "tela2.php?nomePais="+nomePaisRecebido;
        let bandeira = document.getElementById("band_pais");
        let linkBand = document.getElementById("bandeira_pais");
        linkBand.href = paisSelecionado; 
        bandeira.src = dados[0];
        let capitalPais = dados[2];
        let regiaoPais = dados[3];
        let subRegiaoPais = dados[4];
        let populacaoPais = dados[5];
        let linguagemPais = dados[6];
        let vizinhos = dados[7];
        let qntVizinhos = vizinhos.length;
        let respostaVizinhos = "";
        if (qntVizinhos === 0){
            respostaVizinhos = "Esse país não possui vizinhos próximos";
            $('#vizinhos').html(respostaVizinhos).show();
        } else {
            let coluna = 0;
            let newRow = document.createElement('tr');
            for (let contadorVizinhos = 0; contadorVizinhos < qntVizinhos; contadorVizinhos++){
                $.getJSON('funcoes.php?regioes=bandeira&paises='+vizinhos[contadorVizinhos], function(bandeira){
                    if (quantidadeColunatabela === 3){
                        if(coluna < 3){
                            newRow.insertCell(coluna).innerHTML = "<a id='bandeiras_vizinhos' href=tela2.php?nomePais="+vizinhos[contadorVizinhos]+"><img id='band_vizinhos' src="+bandeira+"></a>";
                            coluna ++;
                        } else {
                            newRow = document.createElement('tr');
                            coluna = 0;
                            newRow.insertCell(coluna).innerHTML = "<a id='bandeiras_vizinhos' href=tela2.php?nomePais="+vizinhos[contadorVizinhos]+"><img id='band_vizinhos' src="+bandeira+"></a>";
                            coluna ++;
                        }
                        document.getElementById("tabela_vizinhos").appendChild(newRow);
                    } else {
                        newRow = document.createElement('tr');
                        newRow.insertCell(0).innerHTML = "<a id='bandeiras_vizinhos' href=tela2.php?nomePais="+vizinhos[contadorVizinhos]+"><img id='band_vizinhos' src="+bandeira+"></a>";
                        document.getElementById("tabela_vizinhos").appendChild(newRow);
                    }
			    })
            }
        }
        $('#capital').html(capitalPais).show();
        $('#regiao').html(regiaoPais).show();
        $('#subregiao').html(subRegiaoPais).show();
        $('#populacao').html(populacaoPais).show();
        $('#linguagem').html(linguagemPais).show();
    })
    
    $('#btnVoltar').click(function(){
        window.location.href = "index.php";
    })

});