$(document).ready(function(){

    let regioes = $('#cmbRegiao').val();
    let paises = $('#cmbPais').val();
    let condicaoPegarPais = "";
    let condicaoPegarRegiao = "";
    let larguraTela = window.innerWidth;
    let quantidadeColunatabela = 0;
    if(larguraTela <= 921){
        quantidadeColunatabela = 0;
    } else {
        quantidadeColunatabela = 3;
    }
    reset();
    pegarRegioes(regioes,paises);
    if(regioes === "regioes"){
        condicaoPegarRegiao="paises";
    }
    pegarPaises(condicaoPegarRegiao,paises);

    //<!PEGANDO DADOS OS PAÍSES DA REGIÃO SELECIONADA ->>
    $('#cmbRegiao').change(function(){
        reset();
        regioes = $('#cmbRegiao').val();
        if (regioes === "Escolha uma região"){
            condicaoPegarPais = "paises";
            condicaoPegarRegiao = "regioes";
            pegarRegioes(condicaoPegarRegiao,condicaoPegarPais);
            pegarPaises(condicaoPegarRegiao,condicaoPegarPais);
        } else if(regioes === "regioes"){
            condicaoPegarPais = "paises";
            condicaoPegarRegiao = "regioes";
            pegarRegioes(condicaoPegarRegiao,condicaoPegarPais);
            pegarPaises(condicaoPegarRegiao,condicaoPegarPais);
        } else {
            condicaoPegarPais = "paises";
            pegarPaises(regioes,condicaoPegarPais);
        }
    })
    
    //<!PEGANDO DADOS DO PAIS SELECIONADO ->>
    $('#cmbPais').change(function(){
        reset();
        paises = $('#cmbPais').val();
        if (paises === "Escolha um país"){
            condicaoPegarPais = "paises";
            condicaoPegarRegiao = "regioes";
        } else if(paises === "paises"){
            condicaoPegarPais = "paises";
            condicaoPegarRegiao = "regioes";
        } else {
            $.getJSON('funcoes.php?regioes='+condicaoPegarRegiao+'&paises='+paises, function(retu){
                let newRow = document.createElement('tr');
                newRow.insertCell(0).innerHTML = "<a id='bandeiras' href=tela2.php?nomePais="+paises+"><img id='band' src="+retu[0]+"></a>";
                document.getElementById("tabela").appendChild(newRow);
            })
        }
    })

    //EXIBINDO DADOS SELECIONADOS
    $('#btnPesquisar').click(function(){
        let paisSelecionado = $('#cmbPais').val();
        if(paisSelecionado === "paises" || paisSelecionado === "Escolha um país" || paisSelecionado === ""){
            window.location.href = "index.php";
        } else {
            window.location.href = "tela2.php?nomePais="+paisSelecionado;
        }
    })


    //<!-- FUNÇÃO PARA PEGAR AS REGIÕES -->
    function pegarRegioes(condicaoPegarRegioes,condicaoPegarPaises){
        $.getJSON('funcoes.php?regioes='+condicaoPegarRegioes+'&paises='+condicaoPegarPaises, function(retorno){
            let options = '<option>Escolha uma região</option>';
            for (let i = 0; i < retorno.length; i++) {
                 options += '<option value="' + retorno[i] + '">' + retorno[i] + '</option>';
             }
            $('#cmbRegiao').html(options).show();
        })
    }

    //<!-- FUNÇÃO PARA PEGAR OS PAÍSES -->
    function pegarPaises(condicaoPegarRegiao,condicaoPegarPais){
        if (condicaoPegarRegiao ==="paises" && condicaoPegarPais === "paises"){
            $.getJSON('funcoes.php?regioes='+condicaoPegarRegiao+'&paises='+condicaoPegarPais, function(retorna){
                let opcao = '<option>Escolha um país</option>';
                for (let k = 0; k < retorna.length; k++) {
                    opcao += '<option value="' + retorna[k] + '">' + retorna[k] + '</option>';			
                 }
                $('#cmbPais').html(opcao).show();	
            })
        } else {
            let nomesPaisesRegiao = "";
            $.getJSON('funcoes.php?regioes='+condicaoPegarRegiao+'&paises='+condicaoPegarPais, function(retorna){
                let opcao = '<option>Escolha um país</option>';
                for (let k = 0; k < retorna.length; k++) {
                    opcao += '<option value="' + retorna[k] + '">' + retorna[k] + '</option>';			
                    nomesPaisesRegiao += retorna[k] + "|";
                }
                $('#cmbPais').html(opcao).show();	
                let nomePaisesRegiaoSeparados = nomesPaisesRegiao.split("|");
                let quantidadePaisesRegiao = nomePaisesRegiaoSeparados.length;
                let coluna = 0;
                let newRow = document.createElement('tr');
                for(let contadorBandeiras = 0; contadorBandeiras < quantidadePaisesRegiao; contadorBandeiras++){
                    $.getJSON('funcoes.php?regioes=bandeira&paises='+nomePaisesRegiaoSeparados[contadorBandeiras], function(ret){
                        if (quantidadeColunatabela === 3){
                            if(coluna < 3){
                                newRow.insertCell(coluna).innerHTML = "<a id='bandeiras' href=tela2.php?nomePais="+nomePaisesRegiaoSeparados[contadorBandeiras]+"><img id='band' src="+ret+"></a>";
                                coluna ++;
                            } else {
                                newRow = document.createElement('tr');
                                coluna = 0;
                                newRow.insertCell(coluna).innerHTML = "<a id='bandeiras' href=tela2.php?nomePais="+nomePaisesRegiaoSeparados[contadorBandeiras]+"><img id='band' src="+ret+"></a>";
                                coluna ++;
                            }
                            document.getElementById("tabela").appendChild(newRow);
                        } else {
                            newRow = document.createElement('tr');
                            newRow.insertCell(0).innerHTML = "<a id='bandeiras' href=tela2.php?nomePais="+nomePaisesRegiaoSeparados[contadorBandeiras]+"><img id='band' src="+ret+"></a>";
                            document.getElementById("tabela").appendChild(newRow);
                        }
                    })
                }   
            })      
        }
    }

    //<!-- RFUNÇÃO DE RESET DOS SELECTS -->
		function reset(){
            $('#tabela').empty();
            $('#bandeiras').empty();
            $('#band').empty();
    }
                
});