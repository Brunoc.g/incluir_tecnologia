<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style>
            @import url(css/estiloMobile.css) (max-width:921px);
            @import url(css/estilo.css) (min-width:921px);
        </style>
        <script src="https://code.jquery.com/jquery-2.0.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/funcoesIndex.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                var larguraTela = $(window).width();
                if (largura <= 921) {
                    $('link[href="css/estilo.css"]').attr("href", "estiloMobile.css");
                } else {
                    $('link[href="css/estiloMobile.css"]').attr("href", "estilo.css");
                }
            })
        </script>
    </head>
    <body>
        <div id="cabecalho">
            <div id="img_logo">
                <img src="img/logo.png" width=136.32 height=70>
            </div> 
            <div id="teste"></div>   
        </div>
        <div id="ar_regioes">
            <div id="lb_regioes">
                Região
            </div>
            <select id="cmbRegiao">
            	<option value="regioes">Escolha uma região</option>
			</select>
        </div>
        <div id="ar_paises">
            <div id="lb_paises">
                País
            </div>
            <select id="cmbPais">
            	<option value="paises">Escolha um país</option>
			</select>
        </div>
        <div id="ar_botao">
            <input type="submit" value="PESQUISAR" id="btnPesquisar"/>
        </div> 
        <div id="ar_bandeiras">
            <table id="tabela">
                 <a id="bandeiras"> 
                    <img id="band" style="width: 316px;height: 181px;"/>
                </a>
            </table>
        </div>
    </body>
</html>       