<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <?php
            $nomePais = $_GET['nomePais'];
        ?>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style>
            @import url(css/estiloMobile.css) (max-width:921px);
            @import url(css/estilo.css) (min-width:921px);
        </style>
        <script src="https://code.jquery.com/jquery-2.0.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/funcoesTela2.js"></script>
        <script>
                let nomePaisRecebido = "<?php echo $nomePais; ?>";
        </script>
    </head>

    <body>
        <div id="cabecalho">
            <div id="img_logo">
                <img src="img/logo.png" width=136.32 height=70>
            </div>  
            <div id="ar_botao_voltar">
                <input type="submit" value="VOLTAR" id="btnVoltar"/>
            </div>  
        </div>
        <div id="ar_detalhe_pais">
            <div id="ar_bandeira">
                <a id="bandeira_pais"> 
                    <img id="band_pais"/>
                </a>
            </div>
            <div id="ar_dados_pais">
                <div id="label_nome_pais">Nome:</div>
                <label id="nome" value="<?php echo  $nomePais;?>"><?php echo  $nomePais;?></label>
                <div id="label_capital_pais">Capital:</div>
                <div id="capital">Teste2</div>
                <div id="label_regiao_pais">Região:</div>
                <div id="regiao">Teste3</div>
                <div id="label_sub_regiao_pais">Sub-região:</div>
                <div id="subregiao">Teste4</div>
                <div id="label_populacao_pais">População:</div>
                <div id="populacao">Teste5</div>
                <div id="label_linguagem_pais">Líguas:</div>
                <div id="linguagem">Teste6</div>
            </div>
        </div>
        <div id="ar_label_vizinhos">
            <div id="label_vizinhos_pais">Países vizinhos:</div>
        </div>
        <div id="vizinhos">
            <table id="tabela_vizinhos">
                <a id="bandeiras_vizinhos"> 
                    <img id="band_vizinhos"/>
                </a>
            </table>
        </div>
   </body>

</html>